package com.dr8.xposed.saax;

import android.app.Activity;
import android.app.AndroidAppHelper;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class Xposed implements IXposedHookLoadPackage, IXposedHookZygoteInit {

    private static boolean DEBUG = false;
    private static String TAG = "SAAX: ";
    private static XSharedPreferences prefs;
    private static String SAXXVer = BuildConfig.VERSION_NAME;
    private Activity aaAct;
    private Context aactx;

    private static void log(String tag, String msg) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String formattedDate = df.format(c.getTime());
        XposedBridge.log("[" + formattedDate + "] " + tag + ": " + msg);
    }

    private static void initPrefs() {
        prefs = new XSharedPreferences("com.dr8.xposed.saax", "com.dr8.xposed.saax_preferences");
        prefs.makeWorldReadable();
        DEBUG = prefs.getBoolean("debug", false);
    }

    @Override
    public void initZygote(StartupParam startupParam) throws Throwable {
        initPrefs();
    }

    @Override
    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        initPrefs();

//        String targetcls = "icw";  // found in bzl.java 5.5.602944
//        String targetcls = "lgp";  // found in cog.java 5.6

//        String targetcls2 = "bzm"; // found in bzl.java 5.5.602944
//        String targetcls2 = "coh"; // found in cog.java 5.6

//      a(bool) -- do_nothing to avoid @id/fundip_scrim from ever showing
//        String targetcls3 = "eyk"; 5.1

//      g() -- don't allow call to showLockoutScrim; 4.1 -- do nothing
        String targetcls4 = "com.google.android.gearhead.appdecor.DrawerContentLayout";

//		ba() -- content browse enable speed bump projected - false; "ContentBrowse__enable_speed_bump_projected" 5.7
//      cF() -- content forward browse invisalign default allowed items rotary - int(999); "ContentForwardBrowse__invisalign_default_allowed_items_rotary" 5.7
//      cG() -- content forward browse invisalign default allowed items touch - int(999); "ContentForwardBrowse__invisalign_default_allowed_items_touch" 5.7
//      cH() -- content forward browse invisalign default allowed items touchpad - int(999); "ContentForwardBrowse__invisalign_default_allowed_items_touchpad" 5.7
//		eO() -- "ContentBrowse__sixtap_force_enabled" 5.7
//		eT() -- "ContentBrowse__speedbump_force_enabled" 5.7
//      eU() -- content browse lockout ms - long; "ContentBrowse__lockout_ms" 5.7
//      eV() -- content browse max permits - float; "ContentBrowse__max_permits" 5.7
//      eW() -- content browse permits after lockout - float; "ContentBrowse__permits_after_lockout" 5.7
//      eY() -- content browse permits per sec - float; "ContentBrowse__permits_per_sec" 5.7

//        String targetcls5 = "cof"; // 5.6
        String targetcls5 = "cnz"; // 5.7

//        final String sensortarget1 = "dew"; 5.6  "implements CarSensorManager.CarSensorEventListener"
        final String sensortarget1 = "dey";

//        final String sensortarget2 = "dej"; 5.6 "implements CarSensorManager.CarSensorEventListener"
        final String sensortarget2 = "del";

//        final String sensortarget3 = "dev"; 5.6 '"GH.UnlimitedPrvdr", "Unable to register sensor listener"'
        final String sensortarget3 = "dex";

//        final String sensortarget4 = "ife"; 5.6 '"ADU.AppDecorService", "unexpected sensor update for sensor: %d"'
        final String sensortarget4 = "ihm";

        // 5.5
//        final String sensortarget5 = "neo";
//        final String sensortarget6 = "eau";
//        final String sensortarget7 = "nky";

        final String sensortarget5 = "njf";
        final String sensortarget6 = "ebt";
        final String sensortarget7 = "npr";



        final String targetpkg = "com.google.android.projection.gearhead";

        final String ctxcls = "com.google.android.gearhead.vanagon.VnLaunchPadInternalActivity";

        if (lpparam.packageName.equals(targetpkg)) {
            prefs.reload();
            DEBUG = prefs.getBoolean("debug", false);

            log(TAG, "Hooked Android Auto package");
            if (!DEBUG) log(TAG, "Debug logging disabled in preferences, going silent");

            if (DEBUG) {
                try {
                    Class<?> instrcls = XposedHelpers.findClass("android.app.Instrumentation", lpparam.classLoader);
                    XposedBridge.hookAllMethods(instrcls, "newActivity", new XC_MethodHook() {
                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            super.afterHookedMethod(param);
                            aaAct = (Activity) param.getResult();
                            if (DEBUG) log(TAG, "Current activity: " + aaAct.getClass().getName());
                        }
                    });
                } catch (Throwable t) {
                    if (DEBUG) log(TAG, t.getMessage());
                }
            }

            XposedHelpers.findAndHookMethod(ctxcls, lpparam.classLoader, "onCreate", Bundle.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    aactx = (Context) param.thisObject;
                    if (aactx != null) {
                        try {
                            PackageInfo pInfo = aactx.getPackageManager().getPackageInfo(targetpkg, 0);
                            String aaver = pInfo.versionName;
                            String aaverfixed = aaver.replace("-release","");
                            if (DEBUG) log(TAG, "AA version is: " + aaverfixed);
                            if (DEBUG) log(TAG, "SAAX version is: " + SAXXVer);
                            if (!aaverfixed.equals(SAXXVer)) {
                                if (DEBUG) log(TAG, "Version mismatch; SAAX may not function correctly");
                            }
                        } catch (PackageManager.NameNotFoundException e) {
                            if (DEBUG) log(TAG, e.getMessage());
                        }
                    }
                }
            });

//            XposedHelpers.findAndHookMethod(targetcls, lpparam.classLoader, "a",
//                    String.class, new XC_MethodHook() {
//                        @Override
//                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
//                            String s = (String) param.args[0];
//                            boolean i = (boolean) param.args[1];
//                            if (s.equals("gearhead:passenger_mode_feature_enabled")) {
//                                if (DEBUG) log(TAG, s + " is currently: " + i);
//                                param.args[1] = true;
//                                if (DEBUG) log(TAG, "setting " + s + " to true");
//                            }
//                        }
//                    });

//            XposedHelpers.findAndHookMethod(targetcls2, lpparam.classLoader, "a",
//                    String.class, Boolean.class, new XC_MethodHook() {
//                        @Override
//                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
//                            String s = (String) param.args[0];
//                            boolean i = (boolean) param.args[1];
//                            if (s.equals("gearhead:content_rate_limit_enabled")) {
//                                if (DEBUG) log(TAG, s + " is currently: " + i);
//                                    param.args[1] = false;
//                                if (DEBUG) log(TAG, "setting " + s + " to false");
//                            }
//                            if (s.equals("gearhead:feature_vanagon_unlimited_browse_speed_bump")) {
//                                if (DEBUG) log(TAG, s + " is currently: " + i);
//                                param.args[1] = false;
//                                if (DEBUG) log(TAG, s + " is now: false");
//                            }
//                        }
//                    });
//
//            XposedHelpers.findAndHookMethod(targetcls2, lpparam.classLoader, "a",
//                    String.class, Float.class, new XC_MethodHook() {
//                        @Override
//                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
//                            String s = (String) param.args[0];
//                            float i = (float) param.args[1];
//                            if (s.equals("gearhead:content_browse_permit_per_sec")) {
//                                if (DEBUG) log(TAG, s + " is currently: " + i);
//                                if (DEBUG) log(TAG, "Setting " + s + " to 999F");
//                                param.args[1] = 999F;
//                            }
//                            if (s.equals("gearhead:content_browse_max_stored_permits")) {
//                                if (DEBUG) log(TAG, s + " is currently: " + i);
//                                if (DEBUG) log(TAG, "Setting " + s + " to 999F");
//                                param.args[1] = 999F;
//                            }
//                            if (s.equals("gearhead:content_browse_permits_after_speed_bump")) {
//                                if (DEBUG) log(TAG, s + " is currently: " + i);
//                                if (DEBUG) log(TAG, "Setting " + s + " to 999F");
//                                param.args[1] = 999F;
//                            }
//                        }
//                    });

//            XposedHelpers.findAndHookMethod(targetcls2, lpparam.classLoader, "a",
//                    String.class, Long.class, new XC_MethodHook() {
//                        @Override
//                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
//                            String s = (String) param.args[0];
//                            long i = (long) param.args[1];
//                            if (s.equals("gearhead:content_browse_speed_bump_duration_ms")) {
//                                if (DEBUG) log(TAG, s + " is currently: " + i);
//                                if (DEBUG) log(TAG, "Setting " + s + " to 0L");
//                                param.args[1] = 0L;
//                            }
//                        }
//                    });

//            XposedHelpers.findAndHookMethod(targetcls3, lpparam.classLoader, "a",
//                    "boolean", XC_MethodReplacement.DO_NOTHING);
//
            XposedHelpers.findAndHookMethod(targetcls4, lpparam.classLoader, "g",
                    XC_MethodReplacement.DO_NOTHING);

            XposedHelpers.findAndHookMethod(targetcls5, lpparam.classLoader, "bj",
                    new XC_MethodHook() {
                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            if (DEBUG) log(TAG, "Setting ContentBrowse__enable_speed_bump_projected to false");
                            param.setResult(false);
                        }
                    });

//            XposedHelpers.findAndHookMethod(targetcls5, lpparam.classLoader, "S",
//                    new XC_MethodHook() {
//                        @Override
//                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//                            if (DEBUG) log(TAG, "Setting ContentForwardBrowse__use_speedbump_projected to false");
//                            param.setResult(false);
//                        }
//                    });


//            XposedHelpers.findAndHookMethod(targetcls5, lpparam.classLoader, "dK",
//                    new XC_MethodHook() {
//                        @Override
//                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//                            if (DEBUG) log(TAG, "Setting ContentBrowse__enable_unlimited_browse_when_unknown_speed to true");
//                            param.setResult(true);
//                        }
//                    });

            XposedHelpers.findAndHookMethod(targetcls5, lpparam.classLoader, "cF",
                    new XC_MethodHook() {
                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            if (DEBUG) log(TAG, "Setting ContentForwardBrowse__invisalign_default_allowed_items_rotary to 999");
                            param.setResult(999);
                        }
                    });

            XposedHelpers.findAndHookMethod(targetcls5, lpparam.classLoader, "cG",
                    new XC_MethodHook() {
                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            if (DEBUG) log(TAG, "Setting ContentForwardBrowse__invisalign_default_allowed_items_touch to 999");
                            param.setResult(999);
                        }
                    });

            XposedHelpers.findAndHookMethod(targetcls5, lpparam.classLoader, "cH",
                    new XC_MethodHook() {
                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            if (DEBUG) log(TAG, "Setting ContentForwardBrowse__invisalign_default_allowed_items_touchpad to 999");
                            param.setResult(999);
                        }
                    });

            XposedHelpers.findAndHookMethod(targetcls5, lpparam.classLoader, "eO",
                    new XC_MethodHook() {
                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            if (DEBUG) log(TAG, "Setting ContentBrowse__sixtap_force_enabled to false");
                            param.setResult(false);
                        }
                    });

            XposedHelpers.findAndHookMethod(targetcls5, lpparam.classLoader, "eT",
                    new XC_MethodHook() {
                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            if (DEBUG) log(TAG, "Setting ContentBrowse__speedbump_force_enabled to false");
                            param.setResult(false);
                        }
                    });


            XposedHelpers.findAndHookMethod(targetcls5, lpparam.classLoader, "eU",
                    new XC_MethodHook() {
                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            if (DEBUG) log(TAG, "Setting ContentBrowse__lockout_ms to 0");
                            param.setResult(0L);
                        }
                    });

            XposedHelpers.findAndHookMethod(targetcls5, lpparam.classLoader, "eV",
                    new XC_MethodHook() {
                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            if (DEBUG) log(TAG, "Setting ContentBrowse__max_permits to 999F");
                            param.setResult(999F);
                        }
                    });

            XposedHelpers.findAndHookMethod(targetcls5, lpparam.classLoader, "eW",
                    new XC_MethodHook() {
                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            if (DEBUG) log(TAG, "Setting ContentBrowse__permits_after_lockout to 999F");
                            param.setResult(999F);
                        }
                    });

            XposedHelpers.findAndHookMethod(targetcls5, lpparam.classLoader, "eY",
                    new XC_MethodHook() {
                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            if (DEBUG) log(TAG, "Setting ContentBrowse__permits_per_sec to 999F");
                            param.setResult(999F);
                        }
                    });

            XposedHelpers.findAndHookMethod(sensortarget1, lpparam.classLoader, "a",
                    int.class, long.class, float[].class, byte[].class, new XC_MethodHook() {
                        @Override
                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                            int i = (int) param.args[0];
                            float[] f = (float[]) param.args[2];
                            byte[] b = (byte[]) param.args[3];
                            if (DEBUG) log(TAG, "Overriding sensor data in " + sensortarget1);
                            switch (i) {
                                case 2:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_CAR_SPEED");
                                    f[0] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting speed sensor to 0.0F");
                                    break;
                                case 10:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_LOCATION");
                                    f[4] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting location speed sensor to 0.0F");
                                    break;
                                case 11:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_DRIVING_STATUS");
                                    b[0] = (byte) 0;
                                    if (DEBUG) log(TAG, "Setting driving status byte[0] to 0");
                                    break;
                            }
                        }
                    });

            XposedHelpers.findAndHookMethod(sensortarget2, lpparam.classLoader, "a",
                    int.class, long.class, float[].class, byte[].class, new XC_MethodHook() {
                        @Override
                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                            int i = (int) param.args[0];
                            float[] f = (float[]) param.args[2];
                            byte[] b = (byte[]) param.args[3];
                            if (DEBUG) log(TAG, "Overriding sensor data in " + sensortarget2);
                            switch (i) {
                                case 2:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_CAR_SPEED");
                                    f[0] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting speed sensor to 0.0F");
                                    break;
                                case 10:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_LOCATION");
                                    f[4] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting location speed sensor to 0.0F");
                                    break;
                                case 11:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_DRIVING_STATUS");
                                    b[0] = (byte) 0;
                                    if (DEBUG) log(TAG, "Setting driving status byte[0] to 0");
                                    break;
                            }
                        }
                    });

            XposedHelpers.findAndHookMethod(sensortarget3, lpparam.classLoader, "a",
                    int.class, long.class, float[].class, byte[].class, new XC_MethodHook() {
                        @Override
                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                            int i = (int) param.args[0];
                            float[] f = (float[]) param.args[2];
                            byte[] b = (byte[]) param.args[3];
                            if (DEBUG) log(TAG, "Overriding sensor data in " + sensortarget3);
                            switch (i) {
                                case 2:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_CAR_SPEED");
                                    f[0] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting speed sensor to 0.0F");
                                    break;
                                case 10:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_LOCATION");
                                    f[4] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting location speed sensor to 0.0F");
                                    break;
                                case 11:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_DRIVING_STATUS");
                                    b[0] = (byte) 0;
                                    if (DEBUG) log(TAG, "Setting driving status byte[0] to 0");
                                    break;
                            }
                        }
                    });

            XposedHelpers.findAndHookMethod(sensortarget4, lpparam.classLoader, "a",
                    int.class, long.class, float[].class, byte[].class, new XC_MethodHook() {
                        @Override
                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                            int i = (int) param.args[0];
                            float[] f = (float[]) param.args[2];
                            byte[] b = (byte[]) param.args[3];
                            if (DEBUG) log(TAG, "Overriding sensor data in " + sensortarget4);
                            switch (i) {
                                case 2:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_CAR_SPEED");
                                    f[0] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting speed sensor to 0.0F");
                                    break;
                                case 10:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_LOCATION");
                                    f[4] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting location speed sensor to 0.0F");
                                    break;
                                case 11:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_DRIVING_STATUS");
                                    b[0] = (byte) 0;
                                    if (DEBUG) log(TAG, "Setting driving status byte[0] to 0");
                                    break;
                            }
                        }
                    });

            XposedHelpers.findAndHookMethod(sensortarget5, lpparam.classLoader, "a",
                    int.class, long.class, float[].class, byte[].class, new XC_MethodHook() {
                        @Override
                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                            int i = (int) param.args[0];
                            float[] f = (float[]) param.args[2];
                            byte[] b = (byte[]) param.args[3];
                            if (DEBUG) log(TAG, "Overriding sensor data in " + sensortarget5);
                            switch (i) {
                                case 2:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_CAR_SPEED");
                                    f[0] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting speed sensor to 0.0F");
                                    break;
                                case 10:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_LOCATION");
                                    f[4] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting location speed sensor to 0.0F");
                                    break;
                                case 11:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_DRIVING_STATUS");
                                    b[0] = (byte) 0;
                                    if (DEBUG) log(TAG, "Setting driving status byte[0] to 0");
                                    break;
                            }
                        }
                    });

            XposedHelpers.findAndHookMethod(sensortarget6, lpparam.classLoader, "a",
                    int.class, long.class, float[].class, byte[].class, new XC_MethodHook() {
                        @Override
                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                            int i = (int) param.args[0];
                            float[] f = (float[]) param.args[2];
                            byte[] b = (byte[]) param.args[3];
                            if (DEBUG) log(TAG, "Overriding sensor data in " + sensortarget6);
                            switch (i) {
                                case 2:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_CAR_SPEED");
                                    f[0] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting speed sensor to 0.0F");
                                    break;
                                case 10:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_LOCATION");
                                    f[4] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting location speed sensor to 0.0F");
                                    break;
                                case 11:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_DRIVING_STATUS");
                                    b[0] = (byte) 0;
                                    if (DEBUG) log(TAG, "Setting driving status byte[0] to 0");
                                    break;
                            }
                        }
                    });

            XposedHelpers.findAndHookMethod(sensortarget7, lpparam.classLoader, "a",
                    int.class, long.class, float[].class, byte[].class, new XC_MethodHook() {
                        @Override
                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                            int i = (int) param.args[0];
                            float[] f = (float[]) param.args[2];
                            byte[] b = (byte[]) param.args[3];
                            if (DEBUG) log(TAG, "Overriding sensor data in " + sensortarget7);
                            switch (i) {
                                case 2:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_CAR_SPEED");
                                    f[0] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting speed sensor to 0.0F");
                                    break;
                                case 10:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_LOCATION");
                                    f[4] = 0.0F;
                                    if (DEBUG) log(TAG, "Setting location speed sensor to 0.0F");
                                    break;
                                case 11:
                                    if (DEBUG) log(TAG, "In SENSOR_TYPE_DRIVING_STATUS");
                                    b[0] = (byte) 0;
                                    if (DEBUG) log(TAG, "Setting driving status byte[0] to 0");
                                    break;
                            }
                        }
                    });
        }
    }
}